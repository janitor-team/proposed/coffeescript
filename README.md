## CoffeeScript has moved!

CoffeeScript on NPM has moved to `coffeescript` (no hyphen). Please update references to `coffee-script` to use `coffeescript` instead.

Also, a new major version has been released under the `coffeescript` name. This new release targets modern JavaScript, with minimal breaking changes. Learn more at [http://coffeescript.org](http://coffeescript.org).